import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword,initializeAuth, getReactNativePersistence } from 'firebase/auth';
import { getFirestore, collection, doc, setDoc, getDoc } from 'firebase/firestore';  // Asegúrate de importar la configuración de Firebase
import { firebaseConfig } from '../firebase-config';
import { initializeApp } from 'firebase/app';
import ReactNativeAsyncStorage from '@react-native-async-storage/async-storage';

const app = initializeApp(firebaseConfig);

export const registrarAsistenciaEnFirebase = async (alumnoInfo) => {
    try {
      // Ejemplo de cómo podrías estructurar tu colección en Firebase
      const asistenciaRef = db.collection('asistencia');
  
      // Agregar una entrada en la colección de asistencia
      await asistenciaRef.add({
        nombre: alumnoInfo.nombre,
        matricula: alumnoInfo.matricula,
        fecha: new Date(),
      });
  
      return true; // Éxito al registrar la asistencia
    } catch (error) {
      console.error('Error al registrar la asistencia en Firebase:', error);
      throw error; // Puedes manejar este error según tus necesidades
    }
  };