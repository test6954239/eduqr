// authThunks.js

import { registerUserAPI, loginUserAPI, registerTeacherAPI } from '../api/auth';

export const registerUser = (userData) => {
  return async (dispatch) => {
    try {
      const { user } = await registerUserAPI(userData);
      dispatch({ type: "REGISTER_SUCCESS", payload: { user } });
      // Devuelve un mensaje de éxito
      return "Registro exitoso";
    } catch (error) {
      console.error("Error al registrar el usuario:", error.message);
      dispatch({ type: "REGISTER_FAILURE", payload: error.message });
      throw error;
    }
  };
};

export const registerTeacher = (teacherData) => {
  return async (dispatch) => {
    try {
      console.log("Comenzando registro de maestro");
      const { user } = await registerTeacherAPI(teacherData);
      console.log("Registro de maestro exitoso");
      dispatch({ type: "REGISTER_SUCCESS", payload: { user } });
      // Devuelve un mensaje de éxito
      return "Registro exitoso";
    } catch (error) {
      console.error("Error al registrar el maestro:", error.message);
      dispatch({ type: "REGISTER_FAILURE", payload: error.message });
      throw error;
    }
  };
};


export const loginUser = (email, password) => {
  return async (dispatch) => {
    try {
      const { user, userData } = await loginUserAPI(email, password);
      dispatch({ type: "LOGIN_SUCCESS", payload: { user, userData } });
    } catch (error) {
      console.error("Error durante el inicio de sesión:", error.message);
      dispatch({ type: "LOGIN_FAILURE", payload: error.message });
      throw error;
    }
  };
};
